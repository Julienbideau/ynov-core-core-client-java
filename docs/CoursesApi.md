# CoursesApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**addCourse**](CoursesApi.md#addCourse) | **POST** /api/v1/courses | Ajoute un cours |
| [**addCourseWithHttpInfo**](CoursesApi.md#addCourseWithHttpInfo) | **POST** /api/v1/courses | Ajoute un cours |
| [**deleteCourse**](CoursesApi.md#deleteCourse) | **DELETE** /api/v1/courses/{id} | Supprimer un cours |
| [**deleteCourseWithHttpInfo**](CoursesApi.md#deleteCourseWithHttpInfo) | **DELETE** /api/v1/courses/{id} | Supprimer un cours |
| [**listCourses**](CoursesApi.md#listCourses) | **GET** /api/v1/courses | Liste des cours |
| [**listCoursesWithHttpInfo**](CoursesApi.md#listCoursesWithHttpInfo) | **GET** /api/v1/courses | Liste des cours |



## addCourse

> CourseDto addCourse(courseDto)

Ajoute un cours

Ajoute un nouveau cours en base de données avec un identifiant généré

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        CourseDto courseDto = new CourseDto(); // CourseDto | 
        try {
            CourseDto result = apiInstance.addCourse(courseDto);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#addCourse");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **courseDto** | [**CourseDto**](CourseDto.md)|  | |

### Return type

[**CourseDto**](CourseDto.md)


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

## addCourseWithHttpInfo

> ApiResponse<CourseDto> addCourse addCourseWithHttpInfo(courseDto)

Ajoute un cours

Ajoute un nouveau cours en base de données avec un identifiant généré

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.ApiResponse;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        CourseDto courseDto = new CourseDto(); // CourseDto | 
        try {
            ApiResponse<CourseDto> response = apiInstance.addCourseWithHttpInfo(courseDto);
            System.out.println("Status code: " + response.getStatusCode());
            System.out.println("Response headers: " + response.getHeaders());
            System.out.println("Response body: " + response.getData());
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#addCourse");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Response headers: " + e.getResponseHeaders());
            System.err.println("Reason: " + e.getResponseBody());
            e.printStackTrace();
        }
    }
}
```

### Parameters


| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **courseDto** | [**CourseDto**](CourseDto.md)|  | |

### Return type

ApiResponse<[**CourseDto**](CourseDto.md)>


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |


## deleteCourse

> void deleteCourse(id)

Supprimer un cours

Supprime un cours de la base de données avec son identifiant

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        Long id = 56L; // Long | 
        try {
            apiInstance.deleteCourse(id);
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#deleteCourse");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **Long**|  | |

### Return type


null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

## deleteCourseWithHttpInfo

> ApiResponse<Void> deleteCourse deleteCourseWithHttpInfo(id)

Supprimer un cours

Supprime un cours de la base de données avec son identifiant

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.ApiResponse;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        Long id = 56L; // Long | 
        try {
            ApiResponse<Void> response = apiInstance.deleteCourseWithHttpInfo(id);
            System.out.println("Status code: " + response.getStatusCode());
            System.out.println("Response headers: " + response.getHeaders());
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#deleteCourse");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Response headers: " + e.getResponseHeaders());
            System.err.println("Reason: " + e.getResponseBody());
            e.printStackTrace();
        }
    }
}
```

### Parameters


| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **Long**|  | |

### Return type


ApiResponse<Void>

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |


## listCourses

> List<Course> listCourses()

Liste des cours

Liste les cours existants en base de données

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        try {
            List<Course> result = apiInstance.listCourses();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#listCourses");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**List&lt;Course&gt;**](Course.md)


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

## listCoursesWithHttpInfo

> ApiResponse<List<Course>> listCourses listCoursesWithHttpInfo()

Liste des cours

Liste les cours existants en base de données

### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.ApiResponse;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.CoursesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        CoursesApi apiInstance = new CoursesApi(defaultClient);
        try {
            ApiResponse<List<Course>> response = apiInstance.listCoursesWithHttpInfo();
            System.out.println("Status code: " + response.getStatusCode());
            System.out.println("Response headers: " + response.getHeaders());
            System.out.println("Response body: " + response.getData());
        } catch (ApiException e) {
            System.err.println("Exception when calling CoursesApi#listCourses");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Response headers: " + e.getResponseHeaders());
            System.err.println("Reason: " + e.getResponseBody());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

ApiResponse<[**List&lt;Course&gt;**](Course.md)>


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

