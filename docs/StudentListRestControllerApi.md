# StudentListRestControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**execute**](StudentListRestControllerApi.md#execute) | **GET** /api/v1/student |  |
| [**executeWithHttpInfo**](StudentListRestControllerApi.md#executeWithHttpInfo) | **GET** /api/v1/student |  |



## execute

> List<Student> execute()



### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.StudentListRestControllerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        StudentListRestControllerApi apiInstance = new StudentListRestControllerApi(defaultClient);
        try {
            List<Student> result = apiInstance.execute();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling StudentListRestControllerApi#execute");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**List&lt;Student&gt;**](Student.md)


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

## executeWithHttpInfo

> ApiResponse<List<Student>> execute executeWithHttpInfo()



### Example

```java
// Import classes:
import fr.asys.ynov.core.core.client.invoker.ApiClient;
import fr.asys.ynov.core.core.client.invoker.ApiException;
import fr.asys.ynov.core.core.client.invoker.ApiResponse;
import fr.asys.ynov.core.core.client.invoker.Configuration;
import fr.asys.ynov.core.core.client.invoker.models.*;
import fr.asys.ynov.core.core.client.controllers.StudentListRestControllerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8080");

        StudentListRestControllerApi apiInstance = new StudentListRestControllerApi(defaultClient);
        try {
            ApiResponse<List<Student>> response = apiInstance.executeWithHttpInfo();
            System.out.println("Status code: " + response.getStatusCode());
            System.out.println("Response headers: " + response.getHeaders());
            System.out.println("Response body: " + response.getData());
        } catch (ApiException e) {
            System.err.println("Exception when calling StudentListRestControllerApi#execute");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Response headers: " + e.getResponseHeaders());
            System.err.println("Reason: " + e.getResponseBody());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

ApiResponse<[**List&lt;Student&gt;**](Student.md)>


### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

